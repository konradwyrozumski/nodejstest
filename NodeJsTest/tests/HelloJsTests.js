﻿var assert = require('assert');

describe('HelloWorldTest', function() {
    it('Test1', function () {
        var hello = require('../js/hello.js')();
        assert.ok(hello.world() === "Hello World!\n", "Hello world test pass.");
    });
});
